# README #

UITest issues to track with Xamarin Support

### What is this repository for? ###

* Issues that are to be submitted to Xamarin support
* 2.2.2


### How do I get set up? ###

* Use APK file included in the APK folder
* For missing library references check the Debug\bin folder
* Run the test as specified in Email


### Who do I talk to? ###

* sunit.joshi@hexagon.com
