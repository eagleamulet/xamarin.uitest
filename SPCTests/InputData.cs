﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinTest
{
    public enum LOGIN_KEYS { HOST_NAME, SITE_DIRECTORY, USER_NAME, USER_PWD };
    public class InputData
    {
        private bool _isServer;
        private IDictionary<LOGIN_KEYS, string> _dataDict;
        private IDictionary<LOGIN_KEYS, string> _uiDict;
        public InputData(bool isServer)
        {
            _isServer = isServer;
            _dataDict = new Dictionary<LOGIN_KEYS, string>();
            _uiDict = new Dictionary<LOGIN_KEYS, string>();

            if (_isServer)
            {
                _dataDict.Add(LOGIN_KEYS.HOST_NAME, "spcmobiletest.ingrnet.com");
                _dataDict.Add(LOGIN_KEYS.SITE_DIRECTORY, "mobileora");
                _dataDict.Add(LOGIN_KEYS.USER_NAME, "cmadmin1");
                _dataDict.Add(LOGIN_KEYS.USER_PWD, "cmadmin1");
            }
            else
            {
                _dataDict.Add(LOGIN_KEYS.HOST_NAME, "demo");
                _dataDict.Add(LOGIN_KEYS.SITE_DIRECTORY, "demo");
                _dataDict.Add(LOGIN_KEYS.USER_NAME, "demo");
                _dataDict.Add(LOGIN_KEYS.USER_PWD, "demo");
            }
            _uiDict = new Dictionary<LOGIN_KEYS, string>();
            _uiDict.Add(LOGIN_KEYS.HOST_NAME, "SiteSettingWebHostCmtb-Entry");
            _uiDict.Add(LOGIN_KEYS.SITE_DIRECTORY, "SiteSettingWebDirCmtb-Entry");
            _uiDict.Add(LOGIN_KEYS.USER_NAME, "AuthNameCmtb-Entry");
            _uiDict.Add(LOGIN_KEYS.USER_PWD, "AuthPasswordCmtb-Entry");
        }
        public string UI_HOSTNAME
        {
            get { return _uiDict[LOGIN_KEYS.HOST_NAME]; }
        }
        public string UI_SITE
        {
            get { return _uiDict[LOGIN_KEYS.SITE_DIRECTORY]; }
        }
        public string UI_USER
        {
            get { return _uiDict[LOGIN_KEYS.USER_NAME]; }
        }
        public string UI_PASSWD
        {
            get { return _uiDict[LOGIN_KEYS.USER_PWD]; }
        }

        public string DATA_HOSTNAME
        {
            get { return _dataDict[LOGIN_KEYS.HOST_NAME]; }
        }
        public string DATA_SITE
        {
            get { return _dataDict[LOGIN_KEYS.SITE_DIRECTORY]; }
        }
        public string DATA_USER
        {
            get { return _dataDict[LOGIN_KEYS.USER_NAME]; }
        }
        public string DATA_PASSWD
        {
            get { return _dataDict[LOGIN_KEYS.USER_PWD]; }
        }
    }
}
