﻿using System;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Android;
using SJ.Logging.Interface;
using SJ.Logging.Service;

namespace XamarinTest
{
    [TestFixture]
    public class TestCloud
    {
        AndroidApp app;
        InputData _inputData;
        ILogger<NLog.Logger> _logger;

        [TestFixtureSetUp]
        public void BeforeEachTest()
        {
            //Get the logger from logging factory
            //We need a Console logger since TestCloud logging is 
            //done via Console.WriteLine() method calls
            _logger = LoggingFactory.GetConsoleNLogger();
            _inputData = new InputData(false);

            app = ConfigureApp.Android
            // TODO: Update this path to point to your Android app and uncomment the
            // code if the app is not included in the solution.
            .ApkFile(@"..\..\APK\onsite.apk")
            .StartApp(Xamarin.UITest.Configuration.AppDataMode.Clear);
        }

        private void App_Ok()
        {
            app.WaitForElement(c => c.Id("button2"));
            app.Screenshot("Welcome Dialog");
            //app.Repl();

            app.Tap(c => c.Id("button2"));
            _logger.LogInfo("Tappped Initial OK");
        }

        [Test]
        public void Get_IWPs()
        {
            App_Ok();            
            App_Login();
            App_IWPSList();
        }

        private void App_Login()
        {
            //Webhost Field
            app.Screenshot("After OK Dialog");
            app.WaitForElement(e => e.Class("EntryEditText").Marked(_inputData.UI_HOSTNAME),
                            string.Format("Timed out waiting for {0} field", _inputData.UI_HOSTNAME), TimeSpan.FromSeconds(30));
            app.Tap(x => x.Marked(_inputData.UI_HOSTNAME));
            app.ClearText(x => x.Marked(_inputData.UI_HOSTNAME));
            app.EnterText(e => e.Marked(_inputData.UI_HOSTNAME), _inputData.DATA_HOSTNAME);
            app.DismissKeyboard();

            _logger.LogInfo(String.Format("Entered hostname {0}", _inputData.UI_HOSTNAME));

            //Web directory Field
            app.WaitForElement(e => e.Class("EntryEditText").Marked(_inputData.UI_SITE),
                            String.Format("Timed out waiting for {0} field", _inputData.UI_SITE), TimeSpan.FromSeconds(30));
            app.Tap(e => e.Marked(_inputData.UI_SITE));
            app.ClearText(e => e.Marked(_inputData.UI_SITE));
            app.EnterText(e => e.Marked(_inputData.UI_SITE), _inputData.DATA_SITE);
            app.DismissKeyboard();

            _logger.LogInfo(String.Format("Entered Site {0}", _inputData.UI_SITE));

            //Take screenshot
            app.Screenshot("Site Details");

            //Tap Set Site
            app.Tap(e => e.Class("Button").Marked("ValidateSiteBtn"));
            _logger.LogInfo("Tapped Set Site");

            //Enter user
            app.WaitForElement(x => x.Marked(_inputData.UI_USER), String.Format("Timed out waiting for {0} field", _inputData.UI_USER),
                TimeSpan.FromSeconds(30));
            app.Tap(x => x.Marked(_inputData.UI_USER));
            app.ClearText(x => x.Marked(_inputData.UI_USER));
            app.EnterText(x => x.Marked(_inputData.UI_USER), _inputData.DATA_USER);
            app.DismissKeyboard();

            _logger.LogInfo(String.Format("Entered username {0}", _inputData.UI_USER));

            //Enter user password
            app.WaitForElement(x => x.Marked(_inputData.UI_PASSWD), string.Format("Time out waiting for {0} field", _inputData.UI_PASSWD), TimeSpan.FromSeconds(30));
            app.Tap(e => e.Marked(_inputData.UI_PASSWD));
            app.ClearText(e => e.Marked(_inputData.UI_PASSWD));
            app.EnterText(e => e.Marked(_inputData.UI_PASSWD), _inputData.DATA_PASSWD);
            app.DismissKeyboard();

            //Take screenshot
            app.Screenshot("Login Info");

            //Ok button
            //[Button] label: "LogInBtn",  text: "Log in"
            app.Tap(e => e.Class("Button").Marked("LogInBtn"));
            _logger.LogInfo("Tapped Login");
        }

        private void App_IWPSList()
        {
            //app.Repl();
            app.WaitForElement(x => x.Class("ListView"));
            app.Query(x => x.Class("ListView").Index(0).Descendant()).Count();

            app.Screenshot("After Login");
            _logger.LogInfo("IWPS Loaded");
            
            
            //** This line causes error when run on TestCloud
            var title = app.Query(c => c.Class("ListView").Child().Index(0).Descendant("FormsTextView")).First().Text;
            _logger.LogInfo(String.Format("Testing {0} IWP details...", title));

            //First pass - Tap on the first child
            app.Tap(c => c.Class("ListView").Index(0).Child().Index(0));
            app.WaitForElement(c => c.Class("ListView"));
            app.Screenshot("First Pass"); // + title);
            //Test IWPs Details
            Test_IWPSDetails();
            //Back to main listing
            app.Back();            
        }

        private void Test_IWPSDetails()
        {
            var startTitleHours = Get_TitleHours();
            var firstStepHours = Get_FirstStepHours();
            var isStepChecked = Tap_And_GetFirstStepCheckbox();

            var endTitleHours = Get_TitleHours();
            _logger.LogInfo(String.Format("Start_TitleHours: {0}, End_TitleHours: {1} Step_Hours: {2}, isChecked: {3}",
                                        startTitleHours, endTitleHours, firstStepHours, isStepChecked));
            if (isStepChecked)
            {
                //Should be more by firstStepHours
                Assert.IsTrue(endTitleHours == (startTitleHours + firstStepHours));
            }
            else
            {
                //Should be more by firstStepHours
                Assert.IsTrue(endTitleHours == (startTitleHours - firstStepHours));
            }
        }
        private double Get_TitleHours()
        {            
            //Get the completion value from first entries title
            //Every child of ListView is a LinearLayout element. 
            //Use Descendant since TextView is under another LinearLayout parent
            var elTitleCompletedManHours = app.Query(c => c.Class("ListView").Child("LinearLayout").Index(0).Descendant("TextView").Marked("componentRemainingManHoursTextView")).First();

            return Convert.ToDouble(elTitleCompletedManHours.Text);
        }

        private double Get_FirstStepHours()
        {
            //Get the completion value from first step
            var firstStepCompletedManHours = app.Query(c => c.Class("ListView").Child("LinearLayout").Index(1).Descendant("TextView").Marked("workstepPlannedManHoursTextView")).First();

            return Convert.ToDouble(firstStepCompletedManHours.Text);
        }

        private bool Tap_And_GetFirstStepCheckbox()
        {
            //Get sibling CheckBox
            var isStepChecked = app.Query(c => c.Class("ListView").Child("LinearLayout").Index(1).Descendant("CheckBox").Index(0).Invoke("isChecked").Value<bool>()).First();
            //Tap the CheckBox
            app.Tap(c => c.Class("ListView").Child("LinearLayout").Index(1).Descendant("CheckBox").Index(0));
            return isStepChecked;
        }

        private void Check_Project()
        {
            //Wait for a plant 

            var spf60El = app.WaitForElement(x => x.Marked("SPF60")).SingleOrDefault();
            if (spf60El == null)
                throw new InvalidOperationException("Unable to find SPF60 plant");

            app.Tap("SPF60");
            app.Repl();
            // class - android.widget.TextView
            // resource-id - android:id/message
            // text - Sorry, Smart Construction OnSite is having problems talking with your site.
        }

    }
}

